﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsInventory.Core.Entities
{
    public class Car : IEntity
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public int Year { get; set; }

        public decimal Price { get; set; } 

        public bool New { get; set; }

        public string Owner { get; set; }
    }
}
