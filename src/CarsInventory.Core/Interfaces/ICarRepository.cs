﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarsInventory.Core.Entities;

namespace CarsInventory.Core.Interfaces
{
    public interface ICarRepository
    {
        IEnumerable<Car> GetList(string owner, string search = null);

        Car Get(int id);

        void Insert(Car car);

        void Update(Car car);


    }
}
