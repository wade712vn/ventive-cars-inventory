﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using log4net.Repository.Hierarchy;

namespace CarsInventory.Web.Attributes
{
    public class Log4NetExceptionHandlerAttribute : HandleErrorAttribute
    {
        private readonly ILog _logger = LogManager.GetLogger("ExceptionLogger");

        public override void OnException(ExceptionContext filterContext)
        {
            _logger.Error("Exception thrown", filterContext.Exception);
            base.OnException(filterContext);
        }
    }
}