﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarsInventory.Web.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "User Name is required"), MinLength(5, ErrorMessage = "User Name must have at least 5 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required"), MinLength(7, ErrorMessage = "Password must have at least 7 characters")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Password confirmation Name is required"), Compare("Password", ErrorMessage = "Your password confirmation doesn't match your password")]
        public string ConfirmPassword { get; set; }
    }
}