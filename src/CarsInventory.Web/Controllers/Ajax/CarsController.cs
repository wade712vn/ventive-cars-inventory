﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarsInventory.Core.Entities;
using CarsInventory.Infrastructure;
using CarsInventory.Web.Models;

namespace CarsInventory.Web.Controllers.Ajax
{
    [Authorize]
    public class CarsController : Controller
    {
        [HttpGet]
        [Route("Cars/GetCar/{carId:int}")]
        public ActionResult GetCar(int carId)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var existingCar = unitOfWork.CarRepository.Get(carId);

                if (existingCar == null)
                    return HttpNotFound("Invalid car");

                return Json(existingCar, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Route("Cars/SaveCar")]
        public ActionResult SaveCar(string brand, string model, decimal price, int year, bool? isNew = false)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var currentUser = HttpContext.User.Identity.Name;
                var newCar = new Car()
                {
                    
                    Brand = brand,
                    Model = model,
                    Price = price,
                    Year = year,
                    New = isNew.HasValue && isNew.Value,
                    Owner = currentUser
                };

                unitOfWork.CarRepository.Insert(newCar);
                unitOfWork.SaveChanges();
                return Content(string.Empty);

            }
        }

        [HttpPost]
        [Route("Cars/SaveCar/{carId:int}")]
        public ActionResult SaveCar(int id, string brand, string model, decimal price, int year, bool? isNew = false)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var existingCar = unitOfWork.CarRepository.Get(id);

                if (existingCar == null)
                    return HttpNotFound("Invalid car");

                var currentUser = HttpContext.User.Identity.Name;
                if (existingCar.Owner != currentUser)
                {
                    Response.StatusCode = 400;
                    return Content("You don't have permission to update this car");
                }

                existingCar.Brand = brand;
                existingCar.Model = model;
                existingCar.Price = price;
                existingCar.Year = year;
                existingCar.New = isNew.HasValue && isNew.Value;

                unitOfWork.CarRepository.Update(existingCar);
                unitOfWork.SaveChanges();
                return Content(string.Empty);

            }
        }

    }
}