﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarsInventory.Infrastructure;
using CarsInventory.Web.Models;

namespace CarsInventory.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CarList(string search = null)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var currentUser = User.Identity.Name;

                var cars = unitOfWork.CarRepository.GetList(currentUser, search);
                return PartialView("CarList", cars.Select(x => new CarViewModel()
                {
                    Id = x.Id,
                    Brand = x.Brand,
                    Model = x.Model,
                    Year = x.Year,
                    Price = x.Price,
                    New = x.New
                }).ToList());
            }
        }
    }
}