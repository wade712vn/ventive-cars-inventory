﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CarsInventory.Infrastructure;
using CarsInventory.Infrastructure.Identity;
using CarsInventory.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CarsInventory.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController()
        {
            var context = new CarsInventoryContext();
            var userStore = new UserStore<ApplicationUser>(context);
            userStore.AutoSaveChanges = true;

            _userManager = new UserManager<ApplicationUser>(userStore);
        }

        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            var userName = model.UserName;
            var password = model.Password;

            var user = _userManager.Find(userName, password);

            if (user == null)
            {
                ViewBag.Error = "You have entered an invalid combination of username and password";
                return View();
            }

            LoginUser(userName);

            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private void LoginUser(string userName)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                1, // Ticket version
                userName, // Username associated with ticket
                DateTime.Now, // Date/time issued
                DateTime.Now.AddMinutes(60 * 24), // Date/time to expire
                false, // "true" for a persistent user cookie
                string.Empty, // User-data, in this case the roles
                FormsAuthentication.FormsCookiePath
            );
            string hash = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, // Name of auth cookie
                hash);

            Response.Cookies.Add(cookie);
        }

        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            if (ModelState.IsValid)
            {
                // Validate user name

                var existingUser = _userManager.FindByName(model.UserName);
                if (existingUser == null)
                {
                    var newUser = new ApplicationUser
                    {
                        UserName = model.UserName,
                        SecurityStamp = Guid.NewGuid().ToString()
                    };

                    _userManager.Create(newUser, model.Password);
                    LoginUser(model.UserName);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The user name has already been taken");
                }
                
            }

            ViewBag.Errors = ModelState.Values.SelectMany(x => x.Errors);

            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}