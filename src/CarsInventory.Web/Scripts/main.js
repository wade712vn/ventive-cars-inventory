﻿(function () {
    window.CarsInventory = {
        carList: $("#carList"),
        form: $("#formAddOrEditCar"),
        modalCar: $("#modalAddOrEditCar"),
        showAddOrEditCar: function (carId) {
            var self = this;
            self.modalCar.modal("show");
            self.form.trigger("reset");
            if (carId) {
                self.modalCar.find(".modal-title").html("Edit car");
                self.form.find("input[name=Id]").val(carId);

                var url = "/Cars/GetCar/" + carId;
                this.form.LoadingOverlay("show");
                $.ajax(
                    { url: url, method: "GET" }
                ).then(function (response) {

                    self.form.find("input[name=Brand]").val(response.Brand);
                    self.form.find("input[name=Model]").val(response.Model);
                    self.form.find("input[name=Price]").val(response.Price);
                    self.form.find("input[name=Year]").val(response.Year);
                    self.form.find("input[name=IsNew]").prop("checked", response.New);

                    self.form.LoadingOverlay("hide");

                }, function (errorResponse) {
                    console.log(errorResponse);
                });
            } else {
                self.modalCar.find(".modal-title").html("Add car");

                self.form.find("input[name=Year]").val((new Date()).getFullYear());
                self.form.find("input[name=Price]").val(0.0);
            }
        },
        reloadCarList: function (search) {
            var self = this;
            var url = "/Home/CarList" + (search ? "?search=" + search : "");
            self.carList.LoadingOverlay("show");
            $.ajax(
                { url: url, method: "GET" }
            ).then(function (response) {
                self.carList.html(response);
                self.carList.LoadingOverlay("hide");
            }, function (errorResponse) {
                console.log(errorResponse);
            });
        },
        searchCars: function (event) {
            event.preventDefault();
            var search = $("#textSearch").val();
            this.reloadCarList(search);
        },
        saveCar: function (event) {
            event.preventDefault();

            var carId = $("#hiddenId").val();
            if (carId && carId != 0) {
                this.updateCar(carId);
            } else {
                this.createCar();
            }
        },
        createCar: function () {
            var self = this;
            var url = this.form.prop("action");

            $.ajax(
                { url: url, method: "POST", data: this.form.serialize() }
            ).then(function (response) {
                console.log(response);
                toastr.info("", "Car saved");
                self.modalCar.modal("hide");
                self.reloadCarList();
                $("#textSearch").val("");
            }, function (errorResponse) {
                console.log(errorResponse);
            });
        },
        updateCar: function (carId) {
            var self = this;
            var url = this.form.prop("action") + "/" + carId;

            $.ajax(
                { url: url, method: "POST", data: this.form.serialize() }
            ).then(function (response) {
                console.log(response);
                toastr.info("", "Car saved");
                self.modalCar.modal("hide");
                self.reloadCarList();
                $("#textSearch").val("");
            }, function (errorResponse) {
                console.log(errorResponse);
            });
        },
        getFormData: function ($form) {
            return {
                id: parseInt($form.find("input[name=Id]").val()),
                brand: $form.find("input[name=Brand]").val(),
                model: $form.find("input[name=Model]").val(),
                price: parseFloat($form.find("input[name=Price]").val()),
                year: parseInt($form.find("input[name=Year]").val()),
                isNew: $form.find("input[name=IsNew]").val() === "true"
            };
        }
    };
})();