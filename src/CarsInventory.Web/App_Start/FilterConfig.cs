﻿using System.Web;
using System.Web.Mvc;
using CarsInventory.Web.Attributes;

namespace CarsInventory.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new Log4NetExceptionHandlerAttribute());
        }
    }
}
