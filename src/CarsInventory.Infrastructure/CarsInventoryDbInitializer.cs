﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarsInventory.Infrastructure.Migrations;

namespace CarsInventory.Infrastructure
{
    public class CarsInventoryDbInitializer : MigrateDatabaseToLatestVersion<CarsInventoryContext, Configuration>
    {
    }

    public class CarsInventoryDbInitializerImpl : CarsInventoryDbInitializer
    {
    }
}
