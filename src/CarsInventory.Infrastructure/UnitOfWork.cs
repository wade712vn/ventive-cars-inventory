﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarsInventory.Core.Interfaces;
using CarsInventory.Infrastructure.Repositories;

namespace CarsInventory.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CarsInventoryContext _context;
        private readonly ICarRepository _carRepository;

        public UnitOfWork()
        {
            _context = new CarsInventoryContext();
            _carRepository = new EFCarRepository(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public ICarRepository CarRepository
        {
            get { return _carRepository; }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
