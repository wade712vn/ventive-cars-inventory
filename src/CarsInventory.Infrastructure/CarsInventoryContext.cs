﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarsInventory.Core.Entities;
using CarsInventory.Infrastructure.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CarsInventory.Infrastructure
{
    public class CarsInventoryContext : IdentityDbContext<ApplicationUser>
    {
        public CarsInventoryContext() : base("CarsInventoryDb")
        {
        }

        public CarsInventoryContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public DbSet<Car> Cars { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>()
                .Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);
        }
    }
}
