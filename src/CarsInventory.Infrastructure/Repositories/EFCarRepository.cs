﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarsInventory.Core.Entities;
using CarsInventory.Core.Interfaces;

namespace CarsInventory.Infrastructure.Repositories
{
    public class EFCarRepository : ICarRepository
    {
        private readonly CarsInventoryContext _context;

        public EFCarRepository(CarsInventoryContext context)
        {
            _context = context;
        }

        public IEnumerable<Car> GetList(string owner, string search = null)
        {
            
            return (search == null || search.Trim() == string.Empty) ? _context.Cars.Where(x => x.Owner== owner) : 
                _context.Cars.Where(x => x.Owner == owner && (x.Brand.ToLower().Contains(search.ToLower()) || x.Model.ToLower().Contains(search.ToLower())));
        }

        public Car Get(int id)
        {
            return _context.Cars.Find(id);
        }

        public void Insert(Car car)
        {
            _context.Cars.Add(car);
        }

        public void Update(Car car)
        {
            _context.Entry(car).State = EntityState.Modified;
        }
    }
}
